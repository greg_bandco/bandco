<?php

namespace App;

use Laminas\Diactoros\ServerRequestFactory;
use Bandco\Core\Init\Initializer;
use Roots\Sage\Container;

function error($message, $title = '', $subtitle = '')
{
    $title = $title ?: __('Sage &rsaquo; Error', 'sage');
    $message = "<h1>{$title}<br><small>{$subtitle}</small></h1><p>{$message}</p>";
    wp_die($message, $title);
}

function container()
{
    return Container::getInstance();
}

function request()
{
    return ServerRequestFactory::fromGlobals();
}

Initializer::initRouter(Container::getInstance(), dirname(get_template_directory()), 'routes');
Initializer::initCapsule();

/**
 * Initialisation des classes wordpress par défaut
 */
Initializer::init(Container::getInstance(), [
    '\\Bandco\\Features\\Router',
    '\\Bandco\\Features\\RestApiSecurity',
]);