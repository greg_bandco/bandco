<?php

/*
Plugin Name: BandCo Framework
Plugin URI: https://www.barcelona-co.fr/
Description: Overrider for Sage Theme
Version: 1.0.2
Author: B & Co
Author URI: https://www.barcelona-co.fr/
*/

namespace Bandco;

use Roots\Sage\Container;
use function App\asset_path;

defined('ABSPATH') or die('Nothing here to see!');


define('THEME_DIR_PATH', get_template_directory());
if (!strpos(THEME_DIR_PATH, 'resources')) {
    define('THEME_APP_PATH', THEME_DIR_PATH . '/app');
} else {
    define('THEME_APP_PATH', dirname(THEME_DIR_PATH) . '/app');
}
defined('THEME_TEXTDOMAIN') ? THEME_TEXTDOMAIN : define('THEME_TEXTDOMAIN', 'bandco');
define( 'GF_LICENSE_KEY', '4bd79fee1095854635cde4364f0b9969' );

class bandcoMuPlugins
{
    private static $_instance = null;
    private $c;

    private function __construct()
    {
        add_action('admin_menu', [$this, 'adminMenu']);
        $this->loadSecurity();

        if (strpos(THEME_DIR_PATH, 'resources')) {
            add_action('after_setup_theme', function () {

                $this->c = $this->container();

                $this->loadAjaxParams();

            });

            $this->loadCore();
            $this->getBootstrap();
            $this->loadThemeFiles();
        } elseif (is_dir(THEME_APP_PATH)) {
            add_action('after_setup_theme', function () {

                $this->c = $this->container();

                $this->loadAjaxParams();

            });

            $this->loadCore();
            $this->getBootstrap();
            $this->loadThemeFiles();
        }
    }

    public function loadAjaxParams()
    {
        add_action('wp_enqueue_scripts', function () {
            wp_localize_script('sage/main.js', 'ajaxObject', ['ajaxURL' => admin_url('admin-ajax.php')]);
        }, 100);
    }

    public static function getInstance()
    {
        if (is_null(self::$_instance)) {
            self::$_instance = new bandcoMuPlugins();
        }
        return self::$_instance;
    }

    public function registerSettings()
    {
        $classes = glob(__DIR__ . '/Security/bandco-*.php');
        if (is_array($classes) && count($classes) > 0) {
            foreach ($classes as $classe) {
                register_setting('bandco-mu-plugins-settings', str_replace('.php', '', basename($classe)));
            }
        }
    }

    public function getView()
    {
        require_once(__DIR__ . '/form.html.php');
    }

    public function adminMenu()
    {
        add_submenu_page('options-general.php', 'B&Co Configuration', 'B&Co Configuration', 'administrator',
                'bandco-mu-options', [$this, 'getView']);
        add_action('admin_init', [$this, 'registerSettings']);
    }

    public function loadSecurity()
    {
        $classes = glob(__DIR__ . '/Security/*.php');
        if (is_array($classes) && count($classes) > 0) {
            foreach ($classes as $classe) {
                $opt = str_replace('.php', '', basename($classe));
                if (get_option($opt) === false || intval(get_option($opt)) !== 0) {
                    require_once($classe);
                }
            }
        }
    }

    public function loadCore()
    {
        $classes = glob(__DIR__ . '/Core/*.php');
        if (is_array($classes) && count($classes) > 0) {
            foreach ($classes as $classe) {
                $opt = str_replace('.php', '', basename($classe));
                if (get_option($opt) === false || intval(get_option($opt)) !== 0) {
                    require_once($classe);
                }
            }
        }
    }

    public function container()
    {
        return function_exists('sage') ? Container::getInstance() : \Illuminate\Container\Container::getInstance();
    }

    public function getBootstrap()
    {
        //include WPMU_PLUGIN_DIR.'/bandco/bootstrap.php';
    }

    public function loadThemeFiles()
    {
        $dirs = [
                'Posttype' => scandir(THEME_APP_PATH . '/Posttype'),
                'Taxonomy' => scandir(THEME_APP_PATH . '/Taxonomy'),
                'FieldGroup' => scandir(THEME_APP_PATH . '/FieldGroup'),
                'Features' => scandir(THEME_APP_PATH . '/Features'),
                'Console' => scandir(THEME_APP_PATH . '/Console'),
        ];

        foreach ($dirs as $dir => $files) {
            foreach ($files as $filename) {
                if ($filename != '.' && $filename != '..' && $filename != '.gitkeep') {
                    require_once THEME_APP_PATH . '/' . $dir . '/' . $filename;
                    $objectName = str_replace('.php', '', $filename);
                    if (class_exists($objectName, false)) {
                        $o = new $objectName();
                        if (method_exists($o, 'hooks')) {
                            $o->hooks();
                        }
                    } else {
                        die('Class "' . $objectName . '" not found in ' . THEME_APP_PATH . '/' . $dir . '/' . $this->c);
                    }
                }
            }
        }
    }
}

bandcoMuPlugins::getInstance();
