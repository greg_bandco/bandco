<?php

namespace App\Controllers;

use Sober\Controller\Controller;

/**
 * Gestionnaire des Actions et des Method pour l'ajax
 *
 * ⚠️ Ne pas effacer ce fichier ni le modifier
 */
class AjaxController extends Controller
{
    /**
     * @var string|mixed
     * Fonction utilisée
     */
    private string $postAction = '';

    /**
     * @var string
     * Class de l'ajax
     */
    private string $postMethod = '';

    public function __construct()
    {
        if (isset($_POST[ 'action' ]) && isset($_POST[ 'method' ])) {
            $this->postAction = $_POST[ 'action' ];
            $this->postMethod = 'App\Controllers\Ajax' .ucfirst($_POST[ 'method' ]);


            if (class_exists($this->postMethod) && method_exists($this->postMethod, $this->postAction)) {
                add_action('wp_ajax_' . $this->postAction, [ $this->postMethod , $this->postAction]);
                add_action('wp_ajax_nopriv_' . $this->postAction, [ $this->postMethod, $this->postAction]);
            } else {
                echo '<pre>';
                var_dump('Class or Method App\Controllers\Ajax' . $this->postMethod . '@' . $this->postAction . ' not found.');
                die();
            }
        }
    }
}
