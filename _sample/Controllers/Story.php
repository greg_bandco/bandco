<?php

namespace App\Controllers;

use Sober\Controller\Controller;

class Story extends Controller
{

    public static function getTitle()
    {
        return get_the_title();
    }

    public static function getOnTitle()
    {
        return get_field('story_on_title');
    }

}