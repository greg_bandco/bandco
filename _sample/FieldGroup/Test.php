<?php


use Bandco\Core\WordplateInit;
use JetBrains\PhpStorm\ArrayShape;
use WordPlate\Acf\Fields\ButtonGroup;
use WordPlate\Acf\Fields\Text;
use WordPlate\Acf\Location;

class Test extends WordplateInit
{

    #[ArrayShape(['title' => "string", 'fields' => "array", 'location' => "array"])]
    public function fieldGroup(): array
    {

        return [
                'title' => 'About',
                'fields' => [
                        Text::make('Sur titre', 'story_on_title')
                    ->wrapper([50]),
                ],
                'location' => [
                        Location::if('post_type', 'story')
                ],
        ];
    }
}