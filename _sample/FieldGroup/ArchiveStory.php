<?php


use Bandco\Core\WordplateInit;
use JetBrains\PhpStorm\ArrayShape;
use WordPlate\Acf\Fields\ButtonGroup;
use WordPlate\Acf\Fields\Image;
use WordPlate\Acf\Fields\Text;
use WordPlate\Acf\Location;

class ArchiveStory extends WordplateInit
{

    #[ArrayShape(['title' => "string", 'fields' => "array", 'location' => "array"])]
    public function fieldGroup(): array
    {

        return [
                'title' => 'About Archive',
                'fields' => [
                        Text::make('Sur titre', 'archive_on_title')
                                ->wrapper([50]),
                        Image::make('Image de fond', 'img')
                                ->required(),
                ],
                'location' => [
                        Location::if('options_page', 'options_story')
                ],
        ];
    }
}