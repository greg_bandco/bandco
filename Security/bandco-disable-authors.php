<?php
/**
 * Plugin pour désactiver l'affichages des auteurs et des pages d'archives des auteurs
 */

/* */

class disableAuthors
{
    public function __construct()
    {
        add_action('query_vars', [$this, 'manageQueryVars']);
        add_action('template_redirect', [$this, 'disableAuthorUrl']);
    }

    public function manageQueryVars($qv)
    {
        foreach ($qv as $k => $v) {
            if ($v == 'author' || $v == 'author_name') {
                unset($qv[ $k ]);
            }
        }
        return $qv;
    }

    public function disableAuthorUrl()
    {
        if (is_author()) {
            wp_redirect(home_url());
            exit();
        }
    }
}

new disableAuthors();
/* */
