<?php

/**
 * Plugin qui désactive les commentaires :
 * - Suppression du menu et icone dans l'admin bar
 * - Suppression du menu d'admin
 * - Suppression du support (commentaire et pings ouverts) pour tous les CPT
 */
class LjdDisableComments
{
    public function __construct()
    {
        add_action('admin_init', [$this, 'onAdminInit']);
        add_action('admin_menu', [$this, 'adminMenu']);
        add_action('wp_before_admin_bar_render', [$this, 'adminBar']);
        add_action('init', [$this, 'onInit']);
        // Close comments on the front-end
        add_filter('comments_open', '__return_false', 20, 2);
        add_filter('pings_open', '__return_false', 20, 2);
        // Hide existing comments
        add_filter('comments_array', '__return_empty_array', 10, 2);
        add_filter('wp_count_comments', '__return_false', 20, 2);
        add_action('admin_head', [$this, 'css']);

    }

    public function onAdminInit()
    {
        // Redirect any user trying to access comments page
        global $pagenow;
        if ($pagenow === 'edit-comments.php') {
            wp_redirect(admin_url());
            exit;
        }
        // Remove comments metabox from dashboard
        remove_meta_box('dashboard_recent_comments', 'dashboard', 'normal');
        // Disable support for comments and trackbacks in post types
        foreach (get_post_types() as $post_type) {
            if (post_type_supports($post_type, 'comments')) {
                remove_post_type_support($post_type, 'comments');
                remove_post_type_support($post_type, 'trackbacks');
            }
        }
    }

    public function adminMenu()
    {
        remove_menu_page('edit-comments.php');
        remove_submenu_page('options-general.php', 'options-discussion.php');
    }

    public function onInit()
    {
        if (is_admin_bar_showing()) {
            remove_action('admin_bar_menu', 'wp_admin_bar_comments_menu', 60);
        }
    }

    public function adminBar()
    {
        global $wp_admin_bar;
        $wp_admin_bar->remove_menu('comments');
    }

    public function css()
    {
        // on masque l'entrée dans le widget du dashboard
        echo '<style>#dashboard-widgets .comment-count{display: none}';
    }
}

new LjdDisableComments();
