<?php
/**
 * Plugin pour désactiver l'API et la communication XML-RPC
 * @see http://www.geekpress.fr/desactiver-rest-api-xml-rpc-wordpress/
 */

/* */

class disableApi
{
    public function __construct()
    {
        add_filter('rest_authentication_errors', [$this, 'secureAPI']);
        // -- suppression du XML-RPC
        add_filter('xmlrpc_enabled', '__return_false');
        remove_action('wp_head', 'rsd_link');
    }

    /**
     * Désactive l'API sauf pour les utilisateurs connectés au BO
     */
    public function secureAPI($result)
    {
        if (!empty($result)) {
            return $result;
        }
        if (!is_user_logged_in()) {
            return new WP_Error('rest_not_logged_in', 'You are not currently logged in.', ['status' => 401]);
        }
        return $result;
    }
}

new disableApi();
/* */
