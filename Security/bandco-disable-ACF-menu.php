<?php
/**
 * Plugin pour afficher ou non le menu ACF
 */

/* */

class disableACFMenu
{
    public function __construct()
    {
        add_filter('acf/settings/show_admin', '__return_false');
    }
}

new disableACFMenu();
/* */