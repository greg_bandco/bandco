<?php
/**
 * Plugin pour désactiver le post type "article" de base de WP
 */

/* */

class disableDefaultPost
{
    public function __construct()
    {
        add_action('admin_menu', [$this, 'removeFromMenuAdmin']);
        add_action('admin_init', [$this, 'removeMetaBox']);
        add_action('admin_bar_menu', [$this, 'removeFromAdminBar'], 999);
    }

    public function removeFromMenuAdmin()
    {
        remove_menu_page('edit.php');
    }

    public function removeMetaBox()
    {
        remove_meta_box('dashboard_quick_press', 'dashboard', 'side');
    }

    public function removeFromAdminBar($adminBar)
    {
        $adminBar->remove_menu('new-post');
    }
}

new disableDefaultPost();
/* */
