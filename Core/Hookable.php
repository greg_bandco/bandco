<?php
namespace Bandco\Core;

abstract class Hookable
{
    protected bool $isDisabled = false;
    public array $params = [];

    /**
     * fonction permettant de déclarer les hooks, à surchager pour la declaration
     * @return void
     */
    public function hooks()
    {
    }

    public function init($params = null)
    {
        if (!$this->isDisabled) {
            if (is_array($params)) {
                $this->setParams($params);
            }
            $this->hooks();
        }
    }

    public function setParams(array $params = [])
    {
        foreach ($params as $key => $value) {
            $this->params[$key] = $value;
        }
    }

    public function getParams()
    {
        return $this->params;
    }

    public function getParam($key)
    {
        if (isset($this->params[$key])) {
            return $this->params[$key];
        }
    }
}
