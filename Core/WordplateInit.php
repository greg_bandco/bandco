<?php

namespace Bandco\Core;

abstract class WordplateInit extends Hookable
{
    protected bool $isDisabled = false;

    public function hooks()
    {
        if (!$this->isDisabled) {
            add_action('acf/init', [$this, 'registerFieldGroup']);
        }
    }

    public function fieldGroup(): array
    {
        return [];
    }

    public function registerFieldGroup()
    {
        $fixedParams = [
                'style' => 'default',
                'menu_order' => -1,
                'position' => 'acf_after_title',
        ];

        $params = array_merge($fixedParams, $this->fieldGroup());

        register_extended_field_group($params);
    }
}
