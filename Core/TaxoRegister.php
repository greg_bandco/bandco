<?php

namespace Bandco\Core;

class TaxoRegister extends Hookable
{

    protected bool $isDisabled = false;
    protected string $slug;
    protected string $singular;
    protected string $plural;
    protected array $posttype;
    protected bool $useBtnRadio = false;
    protected bool $useGutenberg = false;
    protected bool $showInMenu = true;
    protected bool $isFemale = false;

    /**
     * @return string
     */
    public function getSingular(): string
    {
        return $this->singular;
    }

    /**
     * @return string
     */
    public function getPlural(): string
    {
        return $this->plural;
    }

    /**
     * @return string
     */
    public function getSlug(): string
    {
        return $this->slug;
    }

    public function hooks()
    {
        if (!$this->isDisabled) {
            add_action('init', [$this, 'registerTaxonomy']);
        }
    }

    public function registerTaxonomy()
    {

        if (empty($this->slug)) {
            return;
        }

        if (empty($this->posttype)) {
            echo 'Vous devez préciser le ou les post_type pour la taxo : ' . $this->slug;
            die();
        }

        $args = [];

        if ($this->useBtnRadio) {
            $args = array_merge($args, [
                # Use radio buttons in the meta box for this taxonomy on the post editing screen:
                    'meta_box' => 'radio',
            ]);
        }

        if ($this->useGutenberg) {
            $args = array_merge($args, [
                    'show_in_rest' => true,
            ]);
        }

        if (!$this->showInMenu) {
            $args = array_merge($args, [
                    'show_in_menu' => false,
            ]);
        }

        /** $names {
         *     Optional. The plural, singular, and slug names.
         *
         * @type string $plural The plural form of the taxonomy name.
         * @type string $singular The singular form of the taxonomy name.
         * @type string $slug The slug used in the term permalinks for this taxonomy.
         * }
         */


        if ($this->isFemale) {
            $labels = [
                    'name' => __($this->getPlural(), THEME_TEXTDOMAIN),
                    'singular_name' => __($this->getSingular(), THEME_TEXTDOMAIN),
                    'search_items' => __('Rechercher une ' . $this->getSingular(), THEME_TEXTDOMAIN),
                    'all_items' => __('Toutes les ' . $this->getPlural(), THEME_TEXTDOMAIN),
                    'parent_item' => __('Parent', THEME_TEXTDOMAIN),
                    'parent_item_colon' => __('Parent :', THEME_TEXTDOMAIN),
                    'edit_item' => __('Éditer la ' . $this->getSingular(), THEME_TEXTDOMAIN),
                    'update_item' => __('Mettre à jour la ' . $this->getSingular(), THEME_TEXTDOMAIN),
                    'add_new_item' => __('Nouvelle ' . $this->getSingular(), THEME_TEXTDOMAIN),
                    'new_item_name' => __('Nouvelle', THEME_TEXTDOMAIN),
                    'menu_name' => __($this->getPlural(), THEME_TEXTDOMAIN),
                    'most_used' => __('Plus utilisées', THEME_TEXTDOMAIN)
            ];
        } else {
            $labels = [
                    'name' => _x($this->getPlural(), THEME_TEXTDOMAIN),
                    'singular_name' => _x($this->getSingular(), THEME_TEXTDOMAIN),
                    'search_items' => __('Rechercher un ' . $this->getSingular(), THEME_TEXTDOMAIN),
                    'all_items' => __('Tous les ' . $this->getPlural(), THEME_TEXTDOMAIN),
                    'parent_item' => __('Parent', THEME_TEXTDOMAIN),
                    'parent_item_colon' => __('Parent :', THEME_TEXTDOMAIN),
                    'edit_item' => __('Éditer le ' . $this->getSingular(), THEME_TEXTDOMAIN),
                    'update_item' => __('Mettre à jour le ' . $this->getSingular(), THEME_TEXTDOMAIN),
                    'add_new_item' => __('Nouveau ' . $this->getSingular(), THEME_TEXTDOMAIN),
                    'new_item_name' => __('Nouveau', THEME_TEXTDOMAIN),
                    'menu_name' => __($this->getPlural(), THEME_TEXTDOMAIN),
                    'most_used' => __('Plus utilisés', THEME_TEXTDOMAIN)
            ];
        }
        $args = array_merge($args, [
                'labels' => $labels,
        ]);
        $names = [
                'plural' => !empty($this->plural) ? $this->plural : $this->slug,
                'singular' => !empty($this->singular) ? $this->singular : $this->slug,
                'slug' => $this->slug,
        ];
        register_extended_taxonomy($this->slug, $this->posttype, $args, $names);
    }
}
