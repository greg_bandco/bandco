<?php

namespace Bandco\Core;

class PostTypeRegister extends Hookable
{
    protected bool $isDisabled = false;
    protected bool $activeGutenberg = true;
    protected string $singular = '';
    protected string $plural = '';
    protected string $slug = '';
    protected bool $isFemale = false;
    protected array $extraArgs = [];
    protected array $commonLabels = [];
    protected array $extraLabels = [];
    protected bool $disableSingle = false;
    protected bool $disableArchivePage = false;
    protected bool $optionPageForArchive = false;
    private static string $getSlug;

    private static string $getSingularName;
    private static string $getPlurarName;
    private string $lowCasePlural = '';
    private string $lowCaseSingular = '';
    private bool $useTags = false;

    /**
     * @return string
     */
    public function getSlug(): string
    {
        return $this->slug;
    }
    /**
     * @return string
     */
    public function getSingularName(): string
    {
        return $this->singular;
    }

    /**
     * @return string
     */
    public function getPlurarName(): string
    {
        return $this->plural;
    }


    public function hooks()
    {


        // @TODO Dev => il faut vérifier les variables slug, singular, plurar
        self::$getSlug = $this->slug;
        self::$getSingularName = $this->singular;
        self::$getPlurarName = $this->plural;

        if (!$this->isDisabled) {
            add_action('init', [$this, 'registerPostType']);
        }
    }


    public function registerPostType()
    {
        $this->lowCaseSingular = strtolower($this->singular);
        $this->lowCasePlural = strtolower($this->plural);

        $labels = array_merge($this->getCommonLabels(), $this->getExtraLabels());

        $support = ['title', 'revisions', 'thumbnail', 'excerpt'];

        if ($this->activeGutenberg) {
            $support = array_merge($support, ['editor']);
        }
        if ($this->useTags) {
            $args['taxonomies'] = ['post_tag'];
        }

        $args = [
                'labels' => $labels,
            # Add the post type to the site's main RSS feed:
                'show_in_feed' => true,

            # Active Gutenberg
                'show_in_rest' => $this->activeGutenberg,
                'supports' => $support,
        ];

        $args = array_merge($args, $this->extraArgs);


        if ($this->disableSingle) {
            $args = array_merge($args, ['publicly_queryable' => false]);
        }


        if ($this->disableArchivePage) {
            $args = array_merge($args, ['has_archive' => false]);
        }



        register_extended_post_type($this->slug, $args, [

            # Override the base names used for labels:
                'singular' => $this->singular,
                'plural' => $this->plural,
                'slug' => $this->slug,

        ]);

        if ($this->optionPageForArchive) {
            $option = "Options";
            acf_add_options_page([
                    'page_title' => $option.' ('.$this->getSingularName().')',
                    'menu_title' => $option,
                    'menu_slug' => 'options_'.$this->getSlug(),
                    'post_id' => 'options_'.$this->getSlug(),
                    'parent_slug' => 'edit.php?post_type='.$this->getSlug()
            ]);
        }
    }

    private function getCommonLabels(): array
    {
        return empty($this->commonLabels) ? ($this->isFemale ? $this->getFemaleLabels() : $this->getMaleLabels()) : $this->commonLabels;
    }

    private function getFemaleLabels(): array
    {
        return [
                'name' => __($this->plural, THEME_TEXTDOMAIN),
                'singular_name' => __($this->singular, THEME_TEXTDOMAIN),
                'menu_name' => __($this->plural, THEME_TEXTDOMAIN),
                'name_admin_bar' => __($this->singular, THEME_TEXTDOMAIN),
                'add_new' => __('Ajouter', THEME_TEXTDOMAIN),
                'add_new_item' => __('Ajouter nouvelle ' . $this->lowCaseSingular, THEME_TEXTDOMAIN),
                'new_item' => __('Nouvelle ' . $this->lowCaseSingular, THEME_TEXTDOMAIN),
                'edit_item' => __('Éditer cette ' . $this->lowCaseSingular, THEME_TEXTDOMAIN),
                'view_item' => __('Voir cette ' . $this->lowCaseSingular, THEME_TEXTDOMAIN),
                'all_items' => __('Toutes les ' . $this->lowCasePlural, THEME_TEXTDOMAIN),
                'search_items' => __('Rechercher une ' . $this->lowCaseSingular, THEME_TEXTDOMAIN),
                'parent_item_colon' => __('Parent recipes:', 'recipe'),
                'not_found' => __('No recipes found.', 'recipe'),
                'not_found_in_trash' => __('No recipes found in Trash.', 'recipe'),
        ];
    }

    private function getMaleLabels(): array
    {
        return [
                'name' => __($this->plural, THEME_TEXTDOMAIN),
                'singular_name' => __($this->singular, THEME_TEXTDOMAIN),
                'menu_name' => __($this->plural, THEME_TEXTDOMAIN),
                'name_admin_bar' => __($this->singular, THEME_TEXTDOMAIN),
                'add_new' => __('Ajouter nouveau', THEME_TEXTDOMAIN),
                'add_new_item' => __('Ajouter nouveau ' . $this->lowCaseSingular, THEME_TEXTDOMAIN),
                'new_item' => __('Nouveau ' . $this->lowCaseSingular, THEME_TEXTDOMAIN),
                'edit_item' => __('Éditer ce ' . $this->lowCaseSingular, THEME_TEXTDOMAIN),
                'view_item' => __('Voir ce ' . $this->lowCaseSingular, THEME_TEXTDOMAIN),
                'all_items' => __('Tous les ' . $this->lowCasePlural, THEME_TEXTDOMAIN),
                'search_items' => __('Rechercher un ' . $this->lowCaseSingular, THEME_TEXTDOMAIN),
                'parent_item_colon' => __('Parent recipes:', 'recipe'),
                'not_found' => __('No recipes found.', 'recipe'),
                'not_found_in_trash' => __('No recipes found in Trash.', 'recipe'),
        ];
    }

    /**
     * @return array
     * Redéfini les labels pour la metabox featured image
     */
    public function getExtraLabels(): array
    {
        if (empty($this->extraLabels)) {
            $this->extraLabels = [
//                'featured_image' => _x(
//                    __('Image principale', THEME_TEXTDOMAIN),
//                    'Overrides the “Featured Image” phrase for this post type. Added in 4.3',
//                    'recipe'
//                ),
//                'set_featured_image' => _x(
//                    'Set cover image',
//                    'Overrides the “Set featured image” phrase for this post type. Added in 4.3',
//                    'recipe'
//                ),
//                'remove_featured_image' => _x(
//                    'Remove cover image',
//                    'Overrides the “Remove featured image” phrase for this post type. Added in 4.3',
//                    'recipe'
//                ),
//                'use_featured_image' => _x(
//                    'Use as cover image',
//                    'Overrides the “Use as featured image” phrase for this post type. Added in 4.3',
//                    'recipe'
//                ),
//                'archives' => _x(
//                    'Recipe archives',
//                    'The post type archive label used in nav menus. Default “Post Archives”. Added in 4.4',
//                    'recipe'
//                ),
//                'insert_into_item' => _x(
//                    'Insert into recipe',
//                    'Overrides the “Insert into post”/”Insert into page” phrase (used when inserting media into a post). Added in 4.4',
//                    'recipe'
//                ),
//                'uploaded_to_this_item' => _x(
//                    'Uploaded to this recipe',
//                    'Overrides the “Uploaded to this post”/”Uploaded to this page” phrase (used when viewing media attached to a post). Added in 4.4',
//                    'recipe'
//                ),
//                'filter_items_list' => _x(
//                    'Filter recipes list',
//                    'Screen reader text for the filter links heading on the post type listing screen. Default “Filter posts list”/”Filter pages list”. Added in 4.4',
//                    'recipe'
//                ),
//                'items_list_navigation' => _x(
//                    'Recipes list navigation',
//                    'Screen reader text for the pagination heading on the post type listing screen. Default “Posts list navigation”/”Pages list navigation”. Added in 4.4',
//                    'recipe'
//                ),
//                'items_list' => _x(
//                    'Recipes list',
//                    'Screen reader text for the items list heading on the post type listing screen. Default “Posts list”/”Pages list”. Added in 4.4',
//                    'recipe'
//                ),
            ];
        }

        return $this->extraLabels;
    }
}
