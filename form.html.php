<div class="wrap">
    <h1>B & Co Configuration</h1>
    <?php $classes = glob(__DIR__.'/Security/bandco-*.php'); ?>
    <?php if (is_array($classes) && count($classes) > 0): ?>
        <form method="post" action="options.php">
            <?php settings_fields('bandco-mu-plugins-settings'); ?>
            <?php do_settings_sections('bandco-mu-plugins-settings'); ?>
            <table class="form-table">
                <?php foreach ($classes as $classe): ?>
                    <?php $classe = str_replace('.php', '', basename($classe)) ?>
                    <tr valign="top">
                        <th scope="row">
                            <?php echo ucfirst(str_replace(['bandco-', '-'], ['', ' '], $classe)); ?>
                        </th>
                        <td>
                            <div class="mnt-radio">
                                <label>
                                    <input type="radio" name="<?php echo $classe ?>"
                                           value="1" <?php echo get_option($classe) === false || intval(get_option($classe)) === 1 ? 'checked' : '' ?> />
                                    actif
                                </label>
                                <label>
                                    <input type="radio" name="<?php echo $classe ?>"
                                           value="0"<?php echo get_option($classe) !== false && intval(get_option($classe)) === 0 ? 'checked' : '' ?> />
                                    inactif
                                </label>
                            </div>
                        </td>
                    </tr>
                <?php endforeach ?>
            </table>
            <?php submit_button(); ?>
        </form>
    <?php endif ?>
</div>
